from django.shortcuts import render
from django.views.generic import FormView, RedirectView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.conf import settings
from .forms import LoginForm
from django.urls import reverse_lazy
from django.shortcuts import redirect

import requests
from requests.exceptions import ConnectionError
from requests_ntlm import HttpNtlmAuth
import os

class LoginView(FormView):
    template_name = 'login/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        username = form.cleaned_data['user']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is None:
            auth=HttpNtlmAuth('{}\\{}'.format(settings.NTLM_DOMAIN, username), password)
            try:
                if settings.NTLM_CERT_PATH:
                    r = requests.get(settings.NTLM_SERVER, verify=settings.NTLM_CERT_PATH, auth=auth)
                else:
                    r = requests.get(settings.NTLM_SERVER, auth=auth)
                if r.status_code == 200:
                    user = User.objects.create_user(username, '', password)
                    user.save()
            except ConnectionError:
                pass
        if user is not None:
            login(self.request, user)
            return redirect(settings.NTLM_REDIRECT)
        return super(LoginView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect(settings.NTLM_REDIRECT)


class LogoutView(RedirectView):
    permanent = False
    pattern_name = 'login'

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            logout(self.request)
        return super(LogoutView, self).get_redirect_url(*args, **kwargs)

